A map representing the Tent of the Hesitants, accompanied by game aids and script interests, revolving around the character of the Hesitant in particular.

All images, excluding maps, from Pixabay.com.
